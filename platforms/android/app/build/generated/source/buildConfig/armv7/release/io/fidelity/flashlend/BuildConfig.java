/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.fidelity.flashlend;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "io.fidelity.flashlend";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "armv7";
  public static final int VERSION_CODE = 62;
  public static final String VERSION_NAME = "0.0.6";
}
