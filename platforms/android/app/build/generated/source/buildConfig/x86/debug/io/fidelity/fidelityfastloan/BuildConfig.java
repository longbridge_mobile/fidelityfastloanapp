/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.fidelity.fidelityfastloan;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "io.fidelity.fidelityfastloan";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "x86";
  public static final int VERSION_CODE = 64;
  public static final String VERSION_NAME = "0.0.6";
}
