import { NgModule } from '@angular/core';
import { LogoutDirective } from './logout/logout';
@NgModule({
	declarations: [LogoutDirective],
	imports: [],
	exports: [LogoutDirective]
})
export class DirectivesModule {}
