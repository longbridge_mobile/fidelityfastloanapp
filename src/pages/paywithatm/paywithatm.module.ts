import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaywithatmPage } from './paywithatm';

@NgModule({
  declarations: [
    PaywithatmPage,
  ],
  imports: [
    IonicPageModule.forChild(PaywithatmPage),
  ],
})
export class PaywithatmPageModule {}
