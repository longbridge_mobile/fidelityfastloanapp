import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PayloanPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-payloan',
  templateUrl: 'payloan.html',
})
export class PayloanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    
  }

  paywithcard () {
    this.navCtrl.push('PaywithcardPage');
  }

  paywithatm() {
    this.navCtrl.push('PaywithatmPage');
  }

  paywithtransfer() {
    this.navCtrl.push('PaywithtransferPage');
  }

  paywithBankTransfer() {
    this.navCtrl.push('DirectdebitPage')
  }



}
