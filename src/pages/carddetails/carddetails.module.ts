import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarddetailsPage } from './carddetails';

@NgModule({
  declarations: [
    CarddetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(CarddetailsPage),
  ],
})
export class CarddetailsPageModule {}
