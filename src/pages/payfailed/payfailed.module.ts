import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PayfailedPage } from './payfailed';

@NgModule({
  declarations: [
    PayfailedPage,
  ],
  imports: [
    IonicPageModule.forChild(PayfailedPage),
  ],
})
export class PayfailedPageModule {}
