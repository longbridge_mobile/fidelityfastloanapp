import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewregPage } from './newreg';

@NgModule({
  declarations: [
    NewregPage,
  ],
  imports: [
    IonicPageModule.forChild(NewregPage),
  ],
})
export class NewregPageModule {}
