import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenaccountsuccessPage } from './openaccountsuccess';

@NgModule({
  declarations: [
    OpenaccountsuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(OpenaccountsuccessPage),
  ],
})
export class OpenaccountsuccessPageModule {}
