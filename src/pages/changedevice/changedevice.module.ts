import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangedevicePage } from './changedevice';

@NgModule({
  declarations: [
    ChangedevicePage,
  ],
  imports: [
    IonicPageModule.forChild(ChangedevicePage),
  ],
})
export class ChangedevicePageModule {}
