import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddownaccountPage } from './addownaccount';

@NgModule({
  declarations: [
    AddownaccountPage,
  ],
  imports: [
    IonicPageModule.forChild(AddownaccountPage),
  ],
})
export class AddownaccountPageModule {}
