import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReferralstatusPage } from './referralstatus';

@NgModule({
  declarations: [
    ReferralstatusPage,
  ],
  imports: [
    IonicPageModule.forChild(ReferralstatusPage),
  ],
})
export class ReferralstatusPageModule {}
