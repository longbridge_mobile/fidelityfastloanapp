import { Component } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { IonicPage, NavController, NavParams, ModalController, ToastController, AlertController} from 'ionic-angular';
import { Loader, Api, Session } from '../../providers';
import { Storage } from '@ionic/storage';
import { HttpHeaders, HttpClient, HttpRequest } from '@angular/common/http';
import { Headers,RequestOptions } from '@angular/http';

/**
 * Generated class for the PaywithcardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-paywithcard',
  templateUrl: 'paywithcard.html',
})
export class PaywithcardPage {

  hasCard: boolean;

  isReadyToProceed: boolean;

  phoneNumber: string;

  sessionKey: string;

  cards: any = [];

  isBusy: boolean = false;

  constructor(public _InAppBrowser: InAppBrowser, public session: Session, public alertCtrl: AlertController, public toastCtrl: ToastController, public api: Api, public storage: Storage, public navCtrl: NavController, public navParams: NavParams, public modal: ModalController, public loading: Loader) {
  }

  ionViewDidLoad() {
    this.phoneNumber = this.session._getPhoneNumber();

    this.sessionKey = this.session._getSessionKey();

    //setTimeout(() => {
      this.getpaymentCards();
    //}, 500);
  }

  showCardalert() {
    let cardAlert =  this.alertCtrl.create({
      title: 'Cards',
      subTitle: 'You currently do not have any card registered with us, kindly add a new card to pay with.',
      buttons: ['Dismiss']
    });
    cardAlert.present();
  }

  doRefresh(refresher) {
    this.getpaymentCards();

    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }


  addCardDetails() {
    this.loading.show('Fetching link...');
    this.api.get('get-cards-registration-link', null, this.api._injectAuth(this.sessionKey)).subscribe((res:any) => {
      this.loading.hide();
      console.log(res);
      if(res.code == '00') {
        let target = "_self";
        //let target = "_blank";
        //let target = "_system";
        var options = "location = yes";
        const browser = this._InAppBrowser.create('https://' + res.message, target, options);
        browser.on('exit').subscribe(event => {
          this.getpaymentCards();
        });
      }
      else {
        this.api.messageHandler(res.message, 5000, 'top');
      }
    }, (error) => {
      this.loading.hide();
      console.log(error);
      this.api.messageHandler(error.error.message);
    })
  }

  getpaymentCards() {
    this.loading.show('Fetching payment cards...');
    this.isBusy = true;
    this.api.get('get-payment-cards', null, this.api._injectAuth(this.sessionKey)).subscribe((res: any) => {
      if(res.code == '00') {
        this.cards = res.data;
      }
      else {
        this.api.messageHandler(res.message, 5000, 'top');
      }
    }, (error: any) => {
      this.api.messageHandler(error.error.message);
    }, () => {
      this.loading.hide();
      this.isBusy = false;
    })
  }

  payWith(card) {
    let pay = this.modal.create('CardlistPage', {card: card});
    pay.onDidDismiss((param ?:any) => {
      if(param) {
        if (!param.success) return;
        this.navCtrl.push('PaysuccessPage', {amountPaid: param.amount, payWithCardResponse: param.res});
      }
    })
    pay.present();
  }

  showCardlist() {
    let cardlist = this.modal.create('CardlistPage', {cards: this.cards});
    cardlist.onDidDismiss((item) => {
      console.log(item);
    })
    cardlist.present();
  }

  /*payLoan () {
    this.loading.show("processing payment...");
    setTimeout(() => {
      this.loading.hide();
      let success = this.modal.create('PaysuccessPage');
      success.onDidDismiss((item) => {
          this.navCtrl.setRoot('DashboardPage');
      })
      success.present();
    }, 5000);
  }*/


  /*addCardDetailsd() {
    let addcard = this.modal.create('CarddetailsPage');
    addcard.onDidDismiss((item) => {
      if(item) {
        this.loading.show("Adding card...");
        setTimeout(() => {
          this.loading.hide();
          console.log(item);
          this.hasCard = true;
          this.isReadyToProceed = true;
        }, 5000);
      }
    })
    addcard.present();
  }*/


}
