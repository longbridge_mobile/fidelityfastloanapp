import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetcardlinkPage } from './getcardlink';

@NgModule({
  declarations: [
    GetcardlinkPage,
  ],
  imports: [
    IonicPageModule.forChild(GetcardlinkPage),
  ],
})
export class GetcardlinkPageModule {}
