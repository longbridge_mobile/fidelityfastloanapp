import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaywithtransferPage } from './paywithtransfer';

@NgModule({
  declarations: [
    PaywithtransferPage,
  ],
  imports: [
    IonicPageModule.forChild(PaywithtransferPage),
  ],
})
export class PaywithtransferPageModule {}
